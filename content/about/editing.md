---
title: "Editing pages"
weight: 10
---

## Colors

Existing colors:

* ddnet.tw sidebar blue: #d8efff
* ddnet.tw links red: #aa4444
* ddnet.tw hove links dark red: #882222
* ddnet logo light orange: #ffa500
* ddnet logo dark orange: #f27400
* ddnet logo light blue: #6ac1ff
* ddnet logo dark blue: #4a9be6
* teeworlds.com background color light blue: #9eb5d6
* default tee: #c3a267
* ddnet-wiki videos: #5e84ae

Helpful resources to pick colors:

* https://material.io/resources/color/#!/?view.left=0&view.right=0
* https://material.io/design/color/text-legibility.html#text-backgrounds
  * High-emphasis text has an opacity of 87% (0xdd)
  * Medium-emphasis text and hint text have opacities of 60% (0x99)
  * Disabled text has an opacity of 38% (0x60)

