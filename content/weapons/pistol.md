---
title: "Pistol"
weight: 20
---

The Pistol is one of the 2 always available weapons. It is semi-automatic weapon and mainly used for communication.
It fires fast bullets, that get pulled slightly down by gravity. The projectile vanishes once it either hits another tee or a wall.
Contrary to the other weapons, the projectiles of the pistol do not affect any tees whatsoever.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Pistol](/images/pistol.png) |
| Crosshair  | ![Pistol crosshair](/images/pistol-crosshair.png) |
| Fire delay | 120ms |

#### Practical Use

In some cases you will want to fire a weapon through a gap between two blocks.
Finding the right angle can be difficult, since it is very precise.

Instead of using the weapon you want to first through the gap to find the angle, use the high fire rate of the pistol instead.
Slightly adjust the angle you fire in until the bullets pass through the gap.  

{{% vid pistol-1 %}}
{{% vid pistol-2 %}}

#### Main Use

The pistol can be used pointer to:

- communicate parts to other tees
- direct the attention of other tees to something

### Advanced Behaviour

- its projectile has no width, meaning it can pass through gaps between blocks
- the projectile will disappear after a short time if it didn't collide beforehand
