---
title: "Game Layer"
explain: true
weight: 20
---

#### Tile representations of the 'game' layer in the editor.

Primary tile layer of maps.
Includes most of the relevant tiles.

{{% explain file="static/explain/entities.svg" %}}
