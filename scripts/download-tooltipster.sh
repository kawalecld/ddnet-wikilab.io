#!/bin/bash

# download to /static/explain/tooltipster
wget https://github.com/iamceege/tooltipster/archive/master.zip
7z e master.zip tooltipster-master/dist/css/tooltipster.bundle.min.css tooltipster-master/dist/js/tooltipster.bundle.min.js
rm master.zip
