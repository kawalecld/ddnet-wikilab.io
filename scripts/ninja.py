#!/usr/bin/env python3

# script for visualizing ninja positions when dashing horizontal
# the tee is starting from the left with the center on the subposition (left number)
# the 'o' are tiles, where the tee passes, all remaining tiles can have freeze on them
# the number on the right is the subposition counting from the right side
# 
# $ ./ninja.py 
#  0>oo oo oo oo o o  29
#  1>oo oo oo oo o o  28
#  2>oo oo oo o oo o  27
#  3>oo oo oo o oo o  26
#  4>oo oo oo o oo o  25
#  5>oo oo oo o oo o  24
#  6>oo oo o oo oo o  23
#  7>oo oo o oo oo o  22
#  8>oo oo o oo oo o  21
#  9>oo oo o oo oo o  20
# 10>oo o oo oo oo o  19
# 11>oo o oo oo oo o  18
# 12>oo o oo oo oo o  17
# 13>oo o oo oo oo o  16
# 14>o oo oo oo oo o  15
# 15>o oo oo oo oo o  14
# 16>o oo oo oo o oo  13
# 17>o oo oo oo o oo  12
# 18>o oo oo oo o oo  11
# 19>o oo oo oo o oo  10
# 20>o oo oo o oo oo  9
# 21>o oo oo o oo oo  8
# 22>o oo oo o oo oo  7
# 23>o oo oo o oo oo  6
# 24>o oo o oo oo oo  5
# 25>o oo o oo oo oo  4
# 26>o oo o oo oo oo  3
# 27>o oo o oo oo oo  2
# 28>o o oo oo oo oo  1
# 29>o o oo oo oo oo  0
# 30>o o oo oo oo o o 31
# 31>o o oo oo oo o o 30

def main():
    grid = [[' ' for _ in range(50*11//32)] for _ in range(32)]
    end = []
    for row in range(32):
        x = row
        for _ in range(10):
            grid[row][x // 32] = 'o'
            x += 50
        end.append(x-50)

    for (i, row) in enumerate(grid):
        if i < 10:
            print(' ', end='')
        print(i, end='>')
        for el in row:
            print(el, end='')
        print(31-end[i] % 32)

if __name__ == '__main__':
    main()
